console.log("Hello World"); //logs a message in console

/*
	Syntax:
		console.log(message);

	Statement:
		console.log("Hello World");
*/


//Writing Comments
	//Comments are meant to describe the written code

//Two Ways of writing comments in JS
/*
	1. Single Line
		// ctrl + /

	2. Multi Line Comment
		(ctrl + shift + /)

*/

/*
	Variables
		- used to contain data. Placeholder
		- when we create variables, certain portion of device memory is given a "name" that we call "variables"

	Declaring Variables
		Syntax:
			let/const variableName;

	let is a keyword usually used in declaring a variable
*/

let myVariable = 500;
console.log(myVariable);

// Variables must be declared first before used
// console.log(hello); //hello is not defined

/*
	Best Practice in Naming Variables
		1. When naming variables, it is important to create variables that are descriptive and indicative of data it contains

		2. When naming variables, it is better to start with lower case

		3. Do not add spaces to variable names. Use camelCase for multiple words
*/

// let firstName = "Mark Joseph"; //good variable name
// let pokemon = 25000; //bad variable name

// let FamilyName = "Mendoza"; //bad variable name;
// let familyName = "Mendoza"; //good variable name;

// let first name = "Jack"; //bad
// let firstName = "Jack"; //good


// Declaring and Initializing Variables
// Initializing variables - instance when variable is given initial/starting value

/*
	Syntax:
		let/const variableName = value
*/

let productName = "desktop computer";
console.log(productName);

let productPrice = 59000;
console.log(productPrice);

const pi = 3.1416;
console.log(pi);

// let and const are keywords that we use to declare a variable
// let - we usually use "let" if we want to reassign the values in our variable

// Reassigning variable values
// Reassigning a variable means changing it's initial or previous value into another value
/*
	Syntax:
		variableName = newValue
*/

productName = 'Laptop';
console.log(productName);

// let student = "Lexus";
// let student = "Jayson"; //Identifier 'student has already been declared'

// const cannot be updated or re-declared
// // Values of constants cannot be changed and will return an error
// pi = 555;
// console.log(pi);

/*
	When to use JS const?
		As general rule, we always declare a variable with const unless we know value will change

	Use const when declare:
		- A new array
		- A new object
		- a new function
*/

// Reassigning variables vs initializing variables
// Declares a variable first
let supplier;
// Initialization is done after variable has been declared
console.log(supplier);
// This is considered initialization b/c it is first time value has been assigned to variable
supplier = "John Smith Tradings";
// let supplier = "John Smith Tradings";
console.log(supplier);
// This is considered reassignment b/c initial value was already declared
supplier = "Zuitt Store";
console.log(supplier);

// Can you declare const value w/o initialization? No. An error will occur
// const interestRate;
// interestRate = 5.15;
// console.log(interestRate)

// Const variables are variables with constant data. We should not re-declare, re-assign, or declare a const variable without initialization

// var vs let/const

// var - is also used in declaring a variable, but var is an ECMAScript1(ES1) feature. Obsolete  

// let/const was introduced as a new feature in ES6 updates 

// What makes let/const different from var?
// let/const is used for readability 
// Example:
	a = 5;
	console.log(a);
	var a;

	// b = 5;
	// console.log(b)
	// let b;

// let/const scope
// let and const are block scope
// block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block

let outerVariable = "Hello";

{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}

console.log(outerVariable);

// var - global scope
var outerVariable1 = "Hello - Var";

{
	var innerVariable1 = "Hello Again - Var";
}

console.log(outerVariable1);
console.log(innerVariable1);

// Multiple Variable Declarations
let productCode = 'DC017', productBrand = 'Dell';
// best practice to separate them
// let productCode = 'DC017'; 
// let productBrand = 'Dell';
console.log(productCode, productBrand);

// Cannot use reserved keyword as a variable name 
// const let = "hello";
// console.log(let);

/*

	DATA TYPES
	
	1. Strings
		- series of characters that create a word, phrase, sentence, or anything related to creating text
		- can be written using single (') or double (") quote

*/

let country = 'Philippines';
let province = "Metro Manila";
console.log(country);
console.log(province);

// Concatenate strings
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// escape character(\) in strings in combination with other characters can produce different effects
// \n - refers to new line between text
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early.';
console.log(message);

message = `John's employees went home early.`;
console.log(message);

// NUMBERS
// Integers/whole (negative/positive)
let headCount = 20;
console.log(headCount);

let numberOfbooks = -1;
console.log(numberOfbooks);

// Decimal/fraction
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text(numbers) and strings
console.log("John's grade last quarter is " + grade);

// Boolean 
// Boolean values are normally used to store values relating to the state of certain things 
// This will be useful in further discussions about creating logic to make our application respond to a certain scenario

let isMarried = false;
let isSingle = true;

console.log("Is 25 years old the marrying age in the Philippines? " + isMarried);
console.log("Is Chiz Escudero not married? " + isMarried);
console.log("Is Liza Soberano single? " + isSingle);

// Arrays
// Arrays are special kind of data type used to store muliple values
// Can store different date types but normally used to store similar data types 
/*
	syntax:
		let/const arrayName = [elementA, elementB, ...]
*/
// starts with 0
let basketOfFruits = ["bananas", "apple", "mango", "strawberry"];
console.log(basketOfFruits);

// similar datatypes 
let grades = [98.7, 77.3, 90.8];
console.log(grades);

// different datatypes
// Storing different data types inside array is not recommended b/c it will not make sense in the context of programming
let random = ["Jungkook", 24, true];
console.log(random);

// Objects
// Another special kind of data type used to mimic real world objects/items
// Used to create complex data that contain pieces of information relevant to each other
// Every individual piece of information is called property of the object
/*
	syntax: 
		let/const objectName = {
			propertyA: valueA,
			propertyB: valueB,
		}
*/
let person = {
	fullName: "Edward Scissorhand",
	age: 35,
	isMarried: false,
	contactNumbers: ["09778121277", "09178921266"],
	address: {
		houseNumber: "345",
		village: "Futura Homes",
		city: "Manila"
	} 
};

console.log(person);

// They're also useful for creating abstract objects
const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGrading: 88.6,
	fourthGrading: 74.9
};

console.log(myGrades);
// typeof operator
// typeof operator is used to determine type of data or value of variable

console.log(typeof myGrades);//object
console.log(typeof grades);//result: object (special type of object)
// note: Array is a special type of object with methods and functions to manipulate it - S22 JS Array Manipulation

// for example:
// const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
// anime = "Kimetsu no yaiba";

// console.log(anime);//Assignment to constant variable

// but if we do this:
const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
console.log(anime[0]);
anime[0] = "Kimetsu no yaiba";
console.log(anime);
// We can change the element of an array assigned to a constant variable
// We can change the object's properties assigned to constant variable

/*
	Constant Object and Array
		keyword const is misleading 
		it does not define a constant value, it defines a constant reference to a value

		you cannot:
		reassign constant value
		reassign contant array
		reassign constant object

		but you can:
		change element of constant array
		change properties of constant object
*/

// Null
// Used to intentionally express absence of value in a variable declaration/initialization

let spouse = null;
console.log(spouse);
// using null compared to 0 value and empty string is much better for readability
// null is considered data type of it's own compared to 0 which is a data type of number and single quotes which a data type of string

let myNumber = 0;
let myString = ' ';

// Undefined 
// Represents state of variable that has been declared without any assigned value 

let sssNumber;
console.log(sssNumber);

/* packages in sublime
 ctrl + shift + p
	- browser sync
	- emmet
	- material theme
*/
